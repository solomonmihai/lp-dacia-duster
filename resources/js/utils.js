function showPopup(popup){
	$(".bg-overlay").show();
	popup.addClass("visible");
	$("body").addClass("no-scroll");
}

function showLoader(){
	$(".preloader").show();
} 

function hideLoader(){
	$(".preloader").fadeOut("slow");
} 

function createCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	}
	else var expires = "";
	document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name, "", -1);
}

function check_scroll(e) {
    var elem = $(e.currentTarget);
    if (Math.floor(elem[0].scrollHeight - elem.scrollTop()) <= elem.outerHeight() + 1000) {
        elem.parent().find(".button").removeAttr("disabled");

        $(".agree").on("click", function(){
            $(".terms").scrollTop(0);
            $(".agree").attr('disabled', true);
            $("body").removeClass("no-scroll");
            $(".pp-body").hide();
            $(".bg-overlay").hide();
            $(".hm-trigger").toggle();
        });
    }
}

function goToByScroll(id){
    id = id.replace("link", "");
    $('html,body').animate({ scrollTop: $("#"+id).offset().top}, 'slow');
}

$(window).on('load', function(){
	$("body").append("<div class=\"preloader\"><div class=\"loader\"></div><span>Loading..</span></div>");
	hideLoader();
});

$(function() {


/* =============================================================================
    o   TERMENI SI CONDITII | POPUP
============================================================================= */


	// Trigger terms popup
	$("#agree").on("click", function(){
		if($("#agree").is(':checked')){
			$("#tandc").addClass("visible");
			if (screen.width < 770) { $(".hm-trigger").toggle(); }
			$("body").addClass("no-scroll");
			$(".bg-overlay").show();
		}
	});

	// Close popup
	$(".close-pp").on("click", function(){
		$("#agree").prop('checked', false);
		$(".agree").attr('disabled', true);
		$(".terms").scrollTop(0);

		$(this).parent().parent().parent().parent().removeClass("visible");
		if (screen.width < 870) { $(".hm-trigger").toggle(); }

		$(".bg-overlay").hide();
		$("body").removeClass("no-scroll");
	});

	// Check scroll
	if ($(".terms").length){
		$('.terms').bind('scroll',check_scroll);
	}

	// Go to bottom
	$(".go_bottom").on("click", function(){
		$('.terms').scrollTop($('.terms')[0].scrollHeight);
	});


/* =============================================================================
	o   CREATE / ACCEPT / HIDE COOKIE
============================================================================= */


	if(readCookie('CookiesNotice')) {
		$(".cookie_disclaimer").hide();
	} else {
		$(".cookie_disclaimer").show();
	}

	$(".accept_terms").click(function (e) {
		createCookie("CookiesNotice", true, 333);
		$(".cookie_disclaimer").hide();
	});


/* =============================================================================
  	o 	SMOOTH SCROLL
============================================================================= */


	// Select all links with hashes
	$('a[href*="#"]')
	// Remove links that don't actually link to anything
	.not('[href="#"]')
	.not('[href="#0"]')
	.click(function (event) {
		  // On-page links
		  if (
			location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
			&&
			location.hostname == this.hostname
			) {
			  // Figure out element to scroll to
		  var target = $(this.hash)
		  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']')
			  // Does a scroll target exist?
			  if (target.length) {
				  // Only prevent default if animation is actually gonna happen
				  event.preventDefault()
				  $('html, body').animate({
					scrollTop: target.offset().top
				  }, 1000, function () {
					  // Callback after animation
					  // Must change focus!
					  var $target = $(target)
					  $target.focus()
					  if ($target.is(':focus')) { // Checking if the target was focused
						return false
					  } else {
						  $target.attr('tabindex', '-1') // Adding tabindex for elements not focusable
						  $target.focus() // Set focus again
					  }

				  })
			  }
		  }
	  });


/* =============================================================================
	o   AUTO INSERT LOADER + BG OVERLAY
============================================================================= */

	$("body").append("<div class=\"bg-overlay\"></div>");


});  

